import { Request, Response } from 'express';
import { db } from '../config/firebaseConfig';

export const updateUser = async (req: Request, res: Response) => {
  const { userId, newData } = req.body;
  try {
    const userRef = db.collection('USERS').doc(userId);
    await userRef.update(newData);
    res.status(200).json({ message: 'User updated successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Error updating user', error });
  }
};

export const fetchUser = async (req: Request, res: Response) => {
  const { userId } = req.query;
  try {
    const userRef = await db.collection('USERS').doc(userId as string).get();
    if (!userRef.exists) {
      return res.status(404).json({ message: 'User not found' });
    }
    res.status(200).json(userRef.data());
  } catch (error) {
    res.status(500).json({ message: 'Error fetching user', error });
  }
};
