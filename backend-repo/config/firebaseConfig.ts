import * as admin from 'firebase-admin';

import * as serviceAccount from './ebuddytest-21f7a-firebase-adminsdk-54t24-50edd6f9fd.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as admin.ServiceAccount)
});

export const db = admin.firestore();
