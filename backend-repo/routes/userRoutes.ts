import express from 'express';
const router = express.Router();

// Import controllers here
import { updateUser, fetchUser } from '../controller/api';

router.put('/update-user-data', updateUser);
router.get('/fetch-user-data', fetchUser);

export default router;
